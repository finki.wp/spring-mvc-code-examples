package mk.ukim.finki.wp.aud1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Aud1Application {

    public static void main(String[] args) {
        SpringApplication.run(Aud1Application.class, args);
    }

}
