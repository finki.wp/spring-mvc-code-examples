package mk.ukim.finki.wp.aud1.web;

import mk.ukim.finki.wp.aud1.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/sample/")
@SessionAttributes("user")
public class SampleController {

    private static List<User> users = new ArrayList<User>();

    @GetMapping(path = "/register")
    public String register() {
        return "register";
    }

    @GetMapping(path = "/user/{name}/**")
    public String showUserInfo(@PathVariable(name="name") String name,
                               Model model) {
        var user = users.stream().filter(v->v.getName().equals(name)).findFirst();
        if (user.isEmpty()) {
            return "redirect:/error";
        }
        model.addAttribute("user", user.get());
        return "index";
    }

    @GetMapping(path="/user/view")
    public String showUserInSession(@SessionAttribute(name = "user") User user,
                                    Model model) {
        if (user == null) {
            return "redirect:/error";
        }
        model.addAttribute("user", user);
        return "index";
    }

    @ModelAttribute
    public void setMsg(Model model) {
        model.addAttribute("msg", "Hello");
    }

    @PostMapping(path = "/register")
    public String registerForm(@RequestParam(name = "name") String n,
                               @RequestParam(name = "surname") String s,
                               @RequestHeader(name="Accept-Language") String acceptLanguage,
                               @CookieValue(name = "JSESSIONID") String jSessionId,
                               Model model)  {
        var user = new User(n, s);
        users.add(user);
        model.addAttribute("user", user);
        return "redirect:/sample/user/view";
    }

    @GetMapping("/sample/logout")
    public String logout(SessionStatus status) {
        status.setComplete();
        return "redirect:/sample/register";
    }

}
