package mk.ukim.finki.wp.aud1.web;

import mk.ukim.finki.wp.aud1.models.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class RestSampleController {

    private static List<User> users = new ArrayList<User>();

    @PutMapping("/register")
    public ResponseEntity<User> register(@RequestBody User user,
                                         UriComponentsBuilder builder) {
        users.add(user);
        URI uri = MvcUriComponentsBuilder.fromController(builder,this.getClass()).path("/{name}").buildAndExpand(user.getName()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/{name}")
    public ResponseEntity<User> view(@PathVariable("name") String name) {
        var user = users.stream().filter(v->v.getName().equals(name)).findFirst();
        if (user.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user.get());
    }

}
